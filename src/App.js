import logo from "./logo.svg";
import "./App.css";
import Amplify, { Storage } from "aws-amplify";
import awsExports from "./aws-exports";
import Button from "@material-ui/core/Button";
import React, { useEffect, useState } from "react";
//import DataGridDemo from './data-grid';
//import BasicList from "./basic-list";
//import * as React from 'react';
import { DataGrid } from "@mui/x-data-grid";
import { AmplifySignOut, withAuthenticator } from "@aws-amplify/ui-react";

Amplify.configure(awsExports);

function App() {
  const [upload_file_name, set_upload_file_name] = useState("");
  const [failed_rows, set_failed_rows] = useState([
    { id: "1", sensor_id: "test_sensor_id", row_count: "row 42" },
  ]);

  function DataGridDemo() {
    const columns = [
      { field: "id", headerName: "ID", width: 120 },

      {
        field: "sensor_id",
        headerName: "Sensor ID",
        width: 150,
      },

      {
        field: "row_count",
        headerName: "Row Number",
        width: 150,
      },
    ];

    return (
      <div
        style={{
          height: 500,
          width: "50%",
          color: "red",
          position: "relative",
        }}
        title="Failed Rows"
      >
        <DataGrid
          rows={failed_rows}
          columns={columns}
          pageSize={10}
          rowsPerPageOptions={[10]}
          disableSelectionOnClick
        />
      </div>
    );
  }

  var lambda_state = "complete";

  function display_loading_icon() {
    if (lambda_state === "processing") {
      document.getElementById("contents").style.visibility = "hidden";
      document.getElementById("load").style.visibility = "visible";
    } else if (lambda_state === "complete") {
      setTimeout(function () {
        document.getElementById("contents").style.visibility = "visible";
        document.getElementById("load").style.visibility = "hidden";
      });
    }
  }

  async function check_sensors() {
    lambda_state = "processing";
    display_loading_icon();

    try {
      var xhr = new XMLHttpRequest();
      xhr.open(
        "GET",
        "https://ppni5ov1k8.execute-api.eu-west-1.amazonaws.com/default/amplify-thomond-convertScript"
      );
      xhr.setRequestHeader("Content-Type", "application/json");

      xhr.onload = function () {
        try {
          const my_response = JSON.parse(this.response);
          const failed_rows = my_response["body"];

          var listItems = [];
          for (const [index, element] of failed_rows.entries()) {
            const myArr = element.split(",");
            listItems.push({
              id: index + 1,
              sensor_id: myArr[0],
              row_count: myArr[1],
            });
          }
          set_failed_rows(listItems);

          console.log(failed_rows);

          alert(
            "The following sensors will fail to update. These could be inactive sensors. If you want to continue, press on PROCESS SENSORS"
          );
          lambda_state = "complete";
          display_loading_icon();

          //set_upload_file_name("");
          document.getElementById("data-grid-div").style.visibility = "visible";

          //document.getElementById("data-grid").style.visibility = "visible";
        } catch (error) {
          lambda_state = "complete";
          display_loading_icon();
          alert("Error Renaming Sensors");

          console.log(
            "error calling the process_file Lambda function: ",
            error.message
          );
        }
      };
      xhr.onerror = function (e) {
        lambda_state = "complete";
        display_loading_icon();
        console.log("error fetching " + e);

        alert("Error fetching " + e);
      };

      xhr.send();
    } catch (error) {
      lambda_state = "complete";
      display_loading_icon();

      console.log(
        "error calling the process_file Lambda function: ",
        error.message
      );
    }
  }

  async function check_xlsx_file() {
    lambda_state = "processing";
    display_loading_icon();

    try {
      var xhr = new XMLHttpRequest();
      xhr.open(
        "GET",
        "https://7b81w93hna.execute-api.eu-west-1.amazonaws.com/default/amplify-thomond-check-file"
      );
      xhr.setRequestHeader("Content-Type", "application/json");

      xhr.onload = function () {
        try {
          const my_response = JSON.parse(this.response);
          const error_msgs = my_response["body"];

          //set_failed_rows(listItems);

          console.log(error_msgs);

          alert("These errors were found" + error_msgs);
          lambda_state = "complete";
          display_loading_icon();

          //set_upload_file_name("");
          //document.getElementById("data-grid-div").style.visibility = "visible";

          //document.getElementById("data-grid").style.visibility = "visible";
        } catch (error) {
          lambda_state = "complete";
          display_loading_icon();
          alert("Error Checking the XLSL File");

          console.log(
            "error calling the amplify-thomond-check-file Lambda function: ",
            error.message
          );
        }
      };
      xhr.onerror = function (e) {
        lambda_state = "complete";
        display_loading_icon();
        console.log("error fetching " + e);

        alert("Error fetching " + e);
      };

      xhr.send();
    } catch (error) {
      lambda_state = "complete";
      display_loading_icon();

      console.log(
        "error calling the process_file Lambda function: ",
        error.message
      );
    }
  }

  async function process_sensors() {
    document.getElementById("data-grid-div").style.visibility = "invisible";

    lambda_state = "processing";
    display_loading_icon();

    try {
      var xhr = new XMLHttpRequest();
      xhr.open(
        "GET",
        "https://dt35udcxle.execute-api.eu-west-1.amazonaws.com/default/amplify-thomond-upload-trigger"
      );
      xhr.setRequestHeader("Content-Type", "application/json");

      xhr.onload = function () {
        try {
          const my_response = JSON.parse(this.response);
          const msg = my_response["body"];

          //set_failed_rows(listItems);

          console.log(msg);

          alert(
            "Renaming of the sensors has started. Please wait, this process can take up to 5 minutes"
          );
          lambda_state = "complete";
          display_loading_icon();
        } catch (error) {
          lambda_state = "complete";
          display_loading_icon();
          alert("Error Renaming the sensors");

          console.log(
            "error calling the process_sensors Lambda function: ",
            error.message
          );
        }
      };
      xhr.onerror = function (e) {
        lambda_state = "complete";
        display_loading_icon();
        console.log("error fetching " + e);

        alert("Error fetching " + e);
      };

      xhr.send();
    } catch (error) {
      lambda_state = "complete";
      display_loading_icon();

      console.log(
        "error calling the process_sensors Lambda function: ",
        error.message
      );
    }
  }

  async function upload_file() {
    lambda_state = "processing";
    display_loading_icon();

    try {
      await Storage.put(upload_file_name.name, upload_file_name, {
        contentType:
          '"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', // contentType is optional
      });
      alert("Successfully Uploaded: " + upload_file_name.name);

      lambda_state = "complete";
      display_loading_icon();

      console.log("successfully uploaded", upload_file_name.name);
    } catch (error) {
      alert("Error Uploading: " + upload_file_name.name);

      console.log("Error uploading file: ", error);

      lambda_state = "complete";
      display_loading_icon();
    }
  }

  async function get_file_name(e) {
    const file = e.target.files[0];
    set_upload_file_name(file);
  }
  return (
    <div id="contents" className="App">
      <header className="App-header">
        <h2>Thomond Sensor Rename App</h2>
        <div id="load"></div>
      </header>

      <body className="App-body">
        <div id="instruction-div">
          <h3>Instructions</h3>
          <p>Step 1: Select the Excel file you want to upload.</p>

          <p>Step 2: Upload the selected file.</p>

          <p>Step 3: Check the uploaded file for mistakes such as duplicated sensor IDs. If there are mistakes found, we recommend fixing
          them before proceeding to the next step, as it might cause errors. However, that is not required.</p>
          
          <p>Step 4: Check the sensors. Some sensors might fail to update, these are displayed in the table. These are probably inactive sensors, 
          but we recommend investigating them before the next step. As before, it is not required and all the other sensors will still updated. </p>
          
          <p>Step 5: Process the sensors. This button actually updates the sensor names. This can take up to 5 minutes. </p>


          
        </div>
        <div id="select-upload-div">
          <input type="file" onChange={get_file_name} />
        </div>

        <div id="upload-div">
          <Button variant="contained" onClick={upload_file}>
            {" "}
            2. Upload File
          </Button>
        </div>

        <div id="check-file-div">
          <Button variant="contained" onClick={check_xlsx_file}>
            {" "}
            3. Check XLSX File
          </Button>
        </div>

        <div id="check-sensors-div">
          <Button variant="contained" onClick={check_sensors}>
            {" "}
            4. Check Sensors{" "}
          </Button>
        </div>

        <div id="process-sensors-div">
          <Button variant="contained" onClick={process_sensors}>
            {" "}
            5. Process Sensors{" "}
          </Button>
        </div>

        <div id="data-grid-div">
          <DataGridDemo />
        </div>

        <div id="signout-div">
          <AmplifySignOut />
        </div>
      </body>
    </div>
  );
}
export default withAuthenticator(App);
